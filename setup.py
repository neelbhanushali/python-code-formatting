# -*- coding: utf-8 -*-
from setuptools import find_packages, setup

with open("requirements.txt") as f:
    install_requires = f.read().strip().split("\n")

# get version from __version__ variable in python_code_formatting/__init__.py
from python_code_formatting import __version__ as version

setup(
    name="python_code_formatting",
    version=version,
    description="Python Code Formatting",
    author="Neel Bhanushali",
    author_email="neel@atriina.com",
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=install_requires,
)
