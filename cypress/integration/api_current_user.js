context("API => currentUser", () => {
  it("currentUser api hit", () => {
    cy.apicall("frappe.auth.get_logged_user").then((res) => {
      expect(res.status).to.eq(200);
      expect(res.body).to.have.property("message", "Guest");
      expect(res.body.message).to.be.a("string");
      cy.screenshot();
    });
  });

  it("currentUser api hit failed", () => {
    cy.apicall("frappe.auth.get_logged_user", {}, "POST", {
      Authorization: "token ee58ee06389535e:3b91d2c09e55169",
    }).then((res) => {
      expect(res.status).to.eq(200);
      expect(res.body).to.have.property("message", "Administrator");
      expect(res.body.message).to.be.a("string");
      cy.screenshot();
    });
  });
});
